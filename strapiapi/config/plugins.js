module.exports = ({ env }) => ({
    // ...
    email: {
        config: {
            provider: 'sendgrid',
            providerOptions: {
                apiKey: env('SENDGRID_API_KEY'),
            },
            settings: {
                defaultFrom: 'strapiadm@gmail.com',
                defaultReplyTo: 'strapiadm@gmail.com',

            },
        },
    },
    // ...
});