module.exports = {
    async afterCreate(event) {
        const { result } = event

        try {
            await strapi.plugins['email'].services.email.send({
                to: 'contacto@digitaltex.cl',
                from: 'strapiadm@gmail.com',
                subject: 'Formulario de contacto Digitaltex',
                text: `Nombre: ${result.name}\nMail: ${result.email}\nMensaje: ${result.message}`,


            })
        } catch (err) {
            console.log(err)
        }
    }
}
