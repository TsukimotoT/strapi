'use strict';

/**
 *  section2 controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::section2.section2');
