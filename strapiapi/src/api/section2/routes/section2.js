'use strict';

/**
 * section2 router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::section2.section2');
