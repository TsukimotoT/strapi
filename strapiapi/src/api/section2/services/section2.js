'use strict';

/**
 * section2 service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::section2.section2');
