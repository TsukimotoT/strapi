'use strict';

/**
 *  navbar-button controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::navbar-button.navbar-button');
