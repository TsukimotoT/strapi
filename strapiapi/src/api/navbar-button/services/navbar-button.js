'use strict';

/**
 * navbar-button service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::navbar-button.navbar-button');
