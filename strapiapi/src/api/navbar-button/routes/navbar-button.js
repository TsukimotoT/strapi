'use strict';

/**
 * navbar-button router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::navbar-button.navbar-button');
