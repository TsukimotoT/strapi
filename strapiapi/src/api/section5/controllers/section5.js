'use strict';

/**
 *  section5 controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::section5.section5');
