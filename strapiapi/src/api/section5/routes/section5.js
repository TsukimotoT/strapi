'use strict';

/**
 * section5 router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::section5.section5');
