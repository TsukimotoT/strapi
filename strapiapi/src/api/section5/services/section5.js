'use strict';

/**
 * section5 service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::section5.section5');
